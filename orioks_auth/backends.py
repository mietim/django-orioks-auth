from django.contrib.auth.models import User

import urllib.response
import urllib.request
import urllib.parse
from urllib.error import URLError


ORIOKS_URL = 'http://orioks.miet.ru/index.php'


class OrioksAuthenticator(object):
    """
    """

    def authenticate_orioks(self, username, password):
        """
        Send authentication request to ORIOKS with provided credentials
        Return True if log in attempt if successful, False otherwise

        :param username: str
        :param password: str
        :return: bool
        """
        try:
            request_data = urllib.parse.urlencode({'TYPE': 'AUTH',
                                                   'AUTH_FORM': 'Y',
                                                   'USER_LOGIN': username,
                                                   'USER_PASSWORD': password}).encode('utf-8')

            request = urllib.request.Request(ORIOKS_URL)
            request.add_header('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8')

            headers = urllib.request.urlopen(request, request_data).info()

            for header in headers.get_all('Set-Cookie'):
                if header.count('BITRIX_SM_LOGIN=' + username):
                    return True

        except URLError:
            pass

        return False

    def authenticate(self, username, password):
        """
        Authenticate user using MIET internal credentials
        Check if user with provided username and password exists in the system
        If not, check ORIOKS for the provided credentials
        Return user object on success, None otherwise

        :param username: str
        :param password: str
        :return: User
        """
        try:
            user = User.objects.get(username=username)

            check_passwd = user.check_password(password)

            if not check_passwd:
                # If user exists, but its password seems to have changed since last login
                # update password
                if not self.authenticate_orioks(username=username, password=password):
                    return None
                user.set_password(password)
                user.save()

        except User.DoesNotExist:
            is_orioks_user = self.authenticate_orioks(username=username, password=password)

            if not is_orioks_user:
                return None

            user = User.objects.create_user(username=username, password=password)

        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
