# django-orioks-auth 1.0

Авторизация в Django, используя МИЭТовскую сеть

## Описание
orioks-auth &ndash; Django приложение, выполняющее всего одну задачу: аутентификацию пользователей из сети МИЭТ. 
Необходимыми данными для авторизации являются номер студенческого билета (или пропуска) и пароль. 

## Установка

* Скачайте содержимое репозитория ```$ git clone git@gitlab.com:mietim/django-orioks-auth.git ```
* Перейдите в папку django-orioks-auth и установите пакет, используя pip ```$ pip install .```
* Добавьте 'orioks_auth' в список установленных приложений:

```python
INSTALLED_APPS = (
	...
	'orioks_auth',
	...
)
```

* Добавьте orioks_auth.backends.OrioksAuthenticator в список authentication backend'ов 

```python
AUTHENTICATION_BACKENDS = (
	...
	'orioks_auth.backends.OrioksAuthenticator',
	...
)
```